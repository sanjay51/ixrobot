package com.ixtutor.ixrobot.physical;

import com.pi4j.io.gpio.*;

/**
 * Created by sanjav on 4/30/17.
 */
public class LED {
    final GpioPinDigitalOutput pin;

    public LED(GpioController gpio, Pin pinNumber) {
        this.pin = gpio.provisionDigitalOutputPin(pinNumber, "MyLED", PinState.LOW);
        this.pin.setShutdownOptions(true, PinState.LOW);
    }

    public void on() {
        this.pin.high();
    }

    public void off() {
        this.pin.low();
    }

    public void toggle() {
        this.pin.toggle();
    }

    public void pulse(long duration) {
        this.pin.pulse(duration);
    }
}
