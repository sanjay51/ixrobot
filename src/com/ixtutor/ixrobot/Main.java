package com.ixtutor.ixrobot;

import com.ixtutor.ixrobot.physical.LED;
import com.pi4j.io.gpio.*;

import static java.lang.Thread.sleep;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("<--Pi4J--> GPIO Control Example ... started.");

        // create gpio controller
        GpioFactory.setDefaultProvider(new RaspiGpioProvider(RaspiPinNumberingScheme.DEFAULT_PIN_NUMBERING));
        final GpioController gpio = GpioFactory.getInstance();

        LED led = new LED(gpio, RaspiPin.GPIO_07);
        led.on();

        System.out.println("--> GPIO state should be: ON");

        sleep(5000);
        led.off();

        // turn off gpio pin #01
        System.out.println("--> GPIO state should be: OFF");

        sleep(5000);

        // toggle the current state of gpio pin #01 (should turn on)
        led.toggle();
        System.out.println("--> GPIO state should be: ON");

        sleep(5000);

        // toggle the current state of gpio pin #01  (should turn off)
        led.toggle();
        System.out.println("--> GPIO state should be: OFF");

        sleep(5000);

        // turn on gpio pin #01 for 1 second and then off
        System.out.println("--> GPIO state should be: ON for only 1 second");
        led.pulse(1000); // set second argument to 'true' use a blocking call

        // stop all GPIO activity/threads by shutting down the GPIO controller
        // (this method will forcefully shutdown all GPIO monitoring threads and scheduled tasks)
        gpio.shutdown();

        System.out.println("Exiting ControlGpioExample");
    }
}
